package question3.back;

public class Player {
	private double money;
	public Player() {
		money = 100;
	}
	public double getMoney() {
		return money;
	}
	public boolean canBet(double bet) {
		return money >= bet;
	}
	public void loss(double deduction) {
		money -= deduction;
	}
	public void win(double win) {
		money += win;
	}
}
