package question3.back;
import java.util.Random;
public class Coin {
	private Random random;
	public Coin() {
		random = new Random();
	}
	public String flipCoin() {
		final String[] COIN = {"heads", "tails"};
		return COIN[random.nextInt(2)];
	}
}
