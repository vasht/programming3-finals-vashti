package question3.client;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import question3.back.Coin;
import question3.back.Player;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class CoinFlip implements EventHandler<ActionEvent>{
	private Form form;
	private ResultsDisplay rd;
	private String choice;
	private Player p;
	private Coin coin;
	
	public CoinFlip(Form form, ResultsDisplay rd, String choice,  Player p) {
		this.form = form;
		this.rd = rd;
		this.choice = choice;
		this.p = p;
		coin = new Coin();
	}
	public void handle(ActionEvent e) {
		boolean isValid = form.validate();
		if(isValid) {
			rd.displayResult(choice,form.getBet(), p, coin);
			form.refresh();
		} else {
			rd.displayInvalid();
		}

	}
}
