package question3.client;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import question3.back.Player;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class CoinFlipClient extends Application{
	public void start(Stage stage) {
		Group root = new Group();
		Scene scene = new Scene(root,650,300);
		VBox overall = new VBox();
		
		Player p = new Player();
		Form form = new Form(p);
		ResultsDisplay rd = new ResultsDisplay();
		
		form.getHeadsButton().setOnAction(new CoinFlip(form, rd,form.getHeadsButton().getText(),p));
		form.getTailsButton().setOnAction(new CoinFlip(form, rd,form.getTailsButton().getText(),p));

		
		overall.getChildren().addAll(form,rd);
		root.getChildren().addAll(overall);
		stage.setTitle("Question 3");
		stage.setScene(scene);
		stage.show();
	}
	public static void main(String[] args) {
		Application.launch(args);
	}
}
