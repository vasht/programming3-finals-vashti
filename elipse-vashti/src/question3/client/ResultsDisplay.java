package question3.client;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import question3.back.Coin;
import question3.back.Player;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class ResultsDisplay extends HBox{
	private Text status;
	private int round;
	public ResultsDisplay() {
		status = new Text();
		round = 1;
		this.getChildren().add(status);
	}
	public void displayInvalid() {
		final String INVALID_MESSAGE = "Invalid bet.";
		status.setText(INVALID_MESSAGE);
	}
	public void displayResult(String choice,double bet, Player p, Coin coin) {
		String coin_result = coin.flipCoin();
		
		if(choice.equals(coin_result)) {
			status.setText("Round " + round + ": Coin landed on " + coin_result + ". You win: $" + bet);
			p.win(bet);
		} else {
			status.setText("Round " + round + ": Coin landed on " + coin_result + ". You lost: $" + bet);
			p.loss(bet);
		}
		round++;
	}
}
