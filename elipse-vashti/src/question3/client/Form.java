package question3.client;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.layout.*;

import question3.back.Player;

public class Form extends HBox{
	
	private Text player_info;
	private Text input_label;
	private TextField input;
	private GridPane gp;
	private Button heads;
	private Button tails;
	private Player player;
	
	public Form(Player p) {
		player = p;
		input_label = new Text("Enter amount: ");
		input = new TextField();
		heads = new Button("heads");
		tails = new Button("tails");
		player_info = new Text("Money: $" + player.getMoney());

		createGridPane();
		this.getChildren().add(gp);
	}
	//This method adds nodes to the GridPane
	private void createGridPane() {
		gp = new GridPane();
		gp.add(input_label, 0, 1);
		gp.add(input, 1, 1);
		gp.add(heads, 0, 2);
		gp.add(tails, 1, 2);
		gp.add(player_info, 0, 0);
	}
	public Button getHeadsButton() {
		return heads;
	}
	public Button getTailsButton() {
		return tails;
	}
	public boolean validate() {
		boolean isValid = true;
		//The bet is valid if it is a number or not empty
		try {
			double bet = Double.parseDouble(input.getText());
			if(!player.canBet(bet) || bet < 0) {
				isValid = false;
			}
		} catch (NumberFormatException | NullPointerException e) {
			isValid = false;
		}
		return isValid;
	}
	public double getBet() {
		return Double.parseDouble(input.getText());
	}
	public void refresh() {
		player_info.setText("Money: $" + player.getMoney());
	}
}
