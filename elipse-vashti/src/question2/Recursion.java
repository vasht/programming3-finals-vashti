package question2;
//Vashti Lanz Rubio(1931765)
public class Recursion {
	public static int recursiveCount(int[] numbers, int n) {
		if(n < 0) {
			n = 0;
		}
		if(n >= numbers.length) {
			return 0;
		}
		return (numbers[n] < 10) && n % 2 == 0 ? recursiveCount(numbers,n+1) + 1 : recursiveCount(numbers,n+1);
	}
	public static void main(String[] args) {
		int[] numbers = {0,1,2,3,4,5,16,7,18,9,7};
		int n = -1;
		System.out.println(recursiveCount(numbers,n));
	}
}

